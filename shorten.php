<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Shortening...</title>
</head>

<body>

<?php

echo "<b>Recieved Values:</b><br />";
echo "Service: ";
if (isset($_POST['service']) && $_POST['service'] != "") {
	$service = $_POST['service'];
	echo $service;
} else {
	echo "invalid service.";
}

echo "<br />Long Url: ";
if (isset($_POST['longUrl']) && $_POST['longUrl'] != "") {
	$longurl = $_POST["longUrl"];
	echo $longurl;
} else {
	echo "invalid longurl.";
}

echo "<br />Custom Short Request: ";
if (isset($_POST['customShortReq']) && $_POST['customShortReq'] != "") {
	$customshortreq = $_POST["customShortReq"];
	echo $customshortreq;
} else {
	echo "invalid customshorturl.";
}

echo "<br /><br /><b>RESPONSE: </b><br /><br />";


// SERVICE SHORTEN


switch($service) {
	case "us.to":
		$link = "http://us.to/api/v1/9d13202122a19e1ce7b7bb0da9199298/shorturl/create/url/" . base64_encode($longurl) . ".json";
		$result = file_get_contents($link);
		echo $result;
		break;
		
	case "is.gd":
		$link = "http://is.gd/create.php?url=" . urlencode($longurl) . "&format=json";
		$result = file_get_contents($link);
		echo "Requested link: " . $link . "<br /><br />";
		echo $result."<br /><br />Result_Processed:<br />";
		$result_processed = json_decode($result);
		echo $result_processed->{'shorturl'};
		break;
		
	case "v.gd":
		$link = "http://v.gd/create.php?url=" . urlencode($longurl) . "&format=json";
		$result = file_get_contents($link);
		echo "Requested link: " . $link . "<br /><br />";
		echo $result."<br /><br />Result_Processed:<br />";
		$result_processed = json_decode($result);
		echo $result_processed->{'shorturl'};
		break;
		
	case "goo.gl":
		include 'googleapi.php';
		echo $json->id;
		break;
		
	case "j.mp":
		include 'bitlyapi.php';
		$result = bitly_v3_shorten($longurl, '9078ec3c5e0c26df6ebf2964a602bcb74a81d085', 'j.mp');
		vardump($result);
		break;
		
	case "snipr.com":
		$link = "http://snipurl.com/site/snip?r=simple&link=" . urlencode($longurl);
		$result = file_get_contents($link);
		echo "Requested link: " . $link . "<br /><br />";
		echo $result;
		break;
		
	case "yep.it":
		$link = "http://yep.it/api.php?url=" . urlencode($longurl);
		$result = file_get_contents($link);
		echo "Requested link: " . $link . "<br /><br />";
		echo $result;
		break;
		
	case "to.ly":
		$link = "http://to.ly/api.php?longurl=" . urlencode($longurl);
		$result = file_get_contents($link);
		echo "Requested link: " . $link . "<br /><br />";
		echo $result;
		break;
		
	case "qlnk.net":
		$link = "http://qlnk.net/api.php?url=" . urlencode($longurl);
		$result = file_get_contents($link);
		echo "Requested link: " . $link . "<br /><br />";
		echo $result;
		break;
		
	case "tiny.cc":
		$link = "http://tiny.cc/?c=rest_api&m=shorten&login=rithvikvibhu&apiKey=0d0a374a-3492-4a1c-9503-7115b3d2ce65&version=2.0.3&format=json&longUrl=" . urlencode($longurl);
		$result = file_get_contents($link);
		echo "Requested link: " . $link . "<br /><br />";
		echo $result."<br /><br />Result_Processed:<br />";
		$result_processed = json_decode($result);
		echo $result_processed->{'results'}->{'short_url'};
		break;
		
	case "ncoq.com":
		$link = "http://ncoq.com/index.php?api=1&return_url_text=1&longUrl=" . urlencode($longurl);
		$result = file_get_contents($link);
		echo "Requested link: " . $link . "<br /><br />";
		echo $result;
		break;
		
	case "4url.fr":
		include 'simple_html_dom.php';
		$html = file_get_contents("http://4url.fr/index.php?u=google.com");
		//echo $html;
		$result = $html->find('div[id=urlbox]');
		echo $result;
		vardump($result);
		break;
		
		case "chilp.it":
		$link = "http://chilp.it/api.php?url=" . urlencode($longurl);
		$result = file_get_contents($link);
		echo "Requested link: " . $link . "<br /><br />";
		echo $result;
		break;
		
		case "migre.me":
		$link = "http://migre.me/api.txt?url=" . urlencode($longurl);
		$result = file_get_contents($link);
		echo "Requested link: " . $link . "<br /><br />";
		echo $result;
		break;
		
		case "qurl.com":
		$link = "http://qurl.com/automate.php?email=rithvikvibhu@gmail.com&url=" . urlencode($longurl);
		$result = file_get_contents($link);
		echo "Requested link: " . $link . "<br /><br />";
		echo $result;
		break;
		
		case "shorl.com":
		$link = "http://shorl.com/create.php?url=" . urlencode($longurl);
		$result = file_get_contents($link);
		echo "Requested link: " . $link . "<br /><br />";
		echo $result;
		break;
		
		case "5.gp":
		$link = "http://5.gp/api/short?longurl=" . urlencode($longurl);
		$result = file_get_contents($link);
		echo "Requested link: " . $link . "<br /><br />";
		echo $result."<br /><br />Result_Processed:<br />";
		$result_processed = json_decode($result);
		echo $result_processed->{'url'};
		break;
		
		case "0.mk":
		$link = "http://api.0.mk/v2/skrati?format=plaintext&link=" . urlencode($longurl);
		$result = file_get_contents($link);
		echo "Requested link: " . $link . "<br /><br />";
		echo $result;
		break;
		
		case "0l.ro":
		$link = "http://api.0l.ro/v1/shorten.php?format=txt&url=" . urlencode($longurl);
		$result = file_get_contents($link);
		echo "Requested link: " . $link . "<br /><br />";
		//vardump($result);
		echo "<iframe scrolling=\"no\" height=\"333px\" width=\"400px\" src=\"" . $link . "\"></iframe>";
		break;
		
		case "tiny":
		include 'openapi.php';
		echo $result;
		break;

}
?>

</body>
</html>