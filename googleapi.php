<?php

$apiKey = 'AIzaSyDBy7w5FfzfKBRdAsxQ93Zpy5VrATHLxO4';
$postData = array('longUrl' => $longurl, 'key' => $apiKey);
$jsonData = json_encode($postData);

$curlObj = curl_init();
curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url');
curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($curlObj, CURLOPT_HEADER, 0);
curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
curl_setopt($curlObj, CURLOPT_POST, 1);
curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);

$response = curl_exec($curlObj);
$json = json_decode($response);
curl_close($curlObj);

?>